# Configuration file.  You should definitely edit this !
# 
# Written by Ian Jackson <ijackson@chiark.greenend.org.uk>, 1999.
# I hereby place this file in the public domain.

$whose= "Cambridge";
$adminname= 'Ian Jackson';
$adminemail= 'ijackson+cam-grin@chiark.greenend.org.uk';
$nameboxlen= 55;
$contactboxlen= 55;
$emailboxlen= 55;
$nullemail= 'discard-all@chiark.greenend.org.uk';

$site= 'http://www.ucam.org';
$whoabbrev= 'cam';
$home= "$site/$whoabbrev-grin/";

$cgisite= 'http://www.chiark.greenend.org.uk';
$user= 'ijackson';
$cgi= "$cgisite/ucgi/~$user/$whoabbrev-grin";

$expire_noconfirm= 48*3600;
$renew_interval= 356*86400;
$expire_norenew= (4*7+6)*86400;

$ENV{'PATH'}= '/usr/local/bin:/bin:/usr/bin';
