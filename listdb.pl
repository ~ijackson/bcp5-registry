# Routines for actually displaying (parts of) database in CGI output.
#
# Copyright (C) 1999 Ian Jackson <ijackson@chiark.greenend.org.uk>
#
# This is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as
# published by the Free Software Foundation; either version 2,
# or (at your option) any later version.
#
# This is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public
# License along with this file; if not, write to the Free Software
# Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

sub list_database ($) {
    my ($instyle) = @_;
    my ($t, $v, $k, $x);
print DEBUG "ldb 0 $instyle\n";
    $instyle =~ m/^all|area|viewoverlap|overlap/ or die "$instyle ?";
    $listing= $&;
    &{"dblist_prep_$listing"}();
    @kl= ();
print DEBUG "ldb 1 $v @kl\n";
    foreach $k (keys %db) {
print DEBUG "ldb q $k\n";
        $ent= $db{$k};
	$dblt_network= $ent->{'network'};
	$dblt_prefix= $ent->{'prefix'};
	next unless &{"dblist_cond_$listing"};
	push @kl,$k;
    }
print DEBUG "ldb 2 @kl\n";
    @kl= sort {
	$x= $db{$a}->{'network'} cmp $db{$b}->{'network'}; return $x if $x;
	$x= $db{$a}->{'prefix'} <=> $db{$b}->{'prefix'}; return $x if $x;
	return -1 if $a eq 'picked' && $b ne 'picked';
	return +1 if $b eq 'picked' && $a ne 'picked';;
	$x= $db{$a}->{'name'} cmp $db{$b}->{'name'}; return $x if $x;
	$x= $db{$a}->{'contact'} cmp $db{$b}->{'contact'}; return $x if $x;
	return $a cmp $b;
    } @kl;
print DEBUG "ldb 3 @kl\n";
    $listingnonefound= @kl ? 0 : 1;
print DEBUG "ldb end $listingnonefound\n";
    $v= "listing$listing"; $$v= 1;
    $list= 1;
}

sub dblist_prep_all { }
sub dblist_cond_all { 1; }

sub dblist_prep_area {
    $dblo_network= $area_networks[$list_areai];
    $dblo_prefix= $area_prefixes[$list_areai];
    $listarea= display_net($dblo_network,$dblo_prefix);
}
sub dblist_cond_area { dblist_cond_overlap(); }

sub dblist_prep_overlap {
    ($dblo_network,$dblo_prefix) = parse_netrange($in{'with'});
    $listoverlap= display_net($dblo_network,$dblo_prefix);
}
sub dblist_cond_overlap {
    return net_overlap($dblt_network,$dblt_prefix, $dblo_network,$dblo_prefix);
}

sub dblist_prep_viewoverlap {
    ($dblo_network,$dblo_prefix) = ($network,$prefix);
    $listoverlap= display_net($network,$prefix);
}
sub dblist_cond_viewoverlap { dblist_cond_overlap(); }

sub foreach_start_db { $db_i=0; }
sub foreach_cond_db { return $db_i < @kl; }
sub foreach_incr_db { $db_i++; }
sub foreach_setvars_db {
    my ($k, $ent);
    $k= $kl[$db_i];
    $ent= $db{$k};
    $dblt_network= $ent->{'network'};
    $dblt_prefix= $ent->{'prefix'};
    $db_picked= $k eq 'picked';
    if ($db_picked) {
	undef $db_id;
    } else {
	$db_id= $k;
    }
    $db_net= display_net($dblt_network,$dblt_prefix);
    if (exists $db{'picked'}) {
	$db_pickedoverlap= net_overlap($dblt_network, $dblt_prefix,
				       $pick_network, $pick_prefix);
    } else {
	$db_pickedoverlap= 0;
    }
    if (defined $id) {
	$db_viewing= ($k eq $id);
    } else {
	$db_viewing= 0;
    }
    $db_name= html_sani($ent->{'name'});
    $db_contact= html_sani($ent->{'contact'});
    $db_hiddenemail= !!$ent->{'hiddenemail'};
    $db_email= $db_hiddenemail ? "" : html_sani($ent->{'email'});
    $db_confirmed= !!$ent->{'changed'};
}

1;
