#!/usr/bin/perl

sub quiz_check_answer () {
    if (! $in{'quiz'}) {
        finish_error('noquiz');
    } elsif ($in{'quiz'} !~ m{^(?:bcp5|rfc1918)$}i) {
        finish_error('badquiz');
    }
}

$quiz_question_input = <<END;
<blockquote>
Quiz question vs. stupid robots:
<br>
Q. Which IETF document,
titled "Address Allocation for Private Internets",
does the G-RIN help implement?
<br>
A. <input type=text name="quiz" length=10>
</blockquote>
END

1;
