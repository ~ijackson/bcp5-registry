#!/bin/sh
# Sample file for installing new Cambridge G-RIN main page.  Of course
# you must edit this.
# 
# Written by Ian Jackson <ijackson@chiark.greenend.org.uk>, 1999.
# I hereby place this file in the public domain.

set -e
file=/home/ijackson/public-html/cam-grin/index.html
./bcp5-registry.pl >$file.new
mv $file.new $file
